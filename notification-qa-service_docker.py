import commands
import mmh3, base64
import sys
from rediscluster import RedisCluster
from flask import Flask
import json
import socket
from cassandra.cluster import Cluster
import aerospike
import pprint

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

app = Flask(__name__)

# Get kafka dump of notification from redis object for th notification id mentioned
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/notification/<host>/<port>/<notification_id>')
def get_notification_info(host, port, notification_id):
    comm = ""
    status = {}
    print host
    r = RedisCluster(host, int(port))
    print notification_id
    status = r.get(notification_id)
    return json.dumps(status)

# Get the redis object for th notification id mentioned
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/pushstatus/<host>/<port>/<notification_id>')
def get_notification_status(host, port, notification_id):
    comm = ""
    status = {}
    print host
    r = RedisCluster(host, int(port))
    print notification_id
    status = r.hgetall('n_' + base64.b64encode(mmh3.hash_bytes('nanlyt_nid_%s' % notification_id))[:-2])
    return json.dumps(status)

# Get capping information
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/capping/<host>/<port>/<key>')
def get_notification_capping_status(host, port, key):
    comm = ""
    status = {}
    connected=False
    maxRetry=5
    r=""
    while not(connected)and (maxRetry>0):
        try:
            r = RedisCluster(host, int(port))
            connected=True
        except Exception as e:
            maxRetry=maxRetry-1
    if not(connected):
        response={}
        response["status"]="Failed"
        response["reason"]="Failed to connect to redis"
        return json.dumps(response)
    k =  "n_" + base64.b64encode(mmh3.hash_bytes(key))[:-2]
    print k
    try:
        smember_set = r.smembers(k)
    except Exception as e:
        print e
        response={}
        response["status"]="Failed"
        response["reason"]="Failed to retrieve data"
        return json.dumps(response) 
    print smember_set
    l = list(smember_set)
    response={}
    response["status"]="Success"
    response["data"]=l
    return json.dumps(response, ensure_ascii=False)

# Delete capping information
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/capping/delete/<host>/<port>/<key>')
def get_notification_capping_delete(host, port, key):
    comm = ""
    status = {}
    connected=False
    maxRetry=5
    r=""
    while not(connected)and (maxRetry>0):
        try:
            r = RedisCluster(host, int(port))
            connected=True
        except Exception as e:
            maxRetry=maxRetry-1
    print "Connected to redis"
    k =  "n_" + base64.b64encode(mmh3.hash_bytes(key))[:-2]
    print k
    count=0
    try:
        count=r.delete(k)
    except Exception as e:
        print e
    response={}
    print "executed"
    if count>0:
        response["status"]="Success"
    else:
        response["status"]="Failed"
    return json.dumps(response)

# Get notification entry from cassandra
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/dbentry/<host>/<channel>/<nid>/<id>')
def get_notification_dbentry(host,channel, nid,id):
    ip=socket.gethostbyname(host)
    print "Connecting to %s" %(str(ip))
    cluster =""
    session =""
    connected=False
    maxRetry=5
    while not(connected)and (maxRetry>0):
        try:
            cluster = Cluster([str(ip)])
            session = cluster.connect("qa_myntra_notifications")
            connected=True
        except Exception as e:
            maxRetry=maxRetry-1
    if not(connected):
        response={}
        response["status"]="Failed"
        response["reason"]="Failed to connect to db"
        return json.dumps(response)
    table=""
    nidcolumn=""
    channel=channel.lower()
    if channel=="inapp":
        table="myntra_all_notifications"
        nidcolumn="masternotificationid"
    elif channel=="beacon":
        table="myntra_beacon_notifications"
        nidcolumn="notificationid"
    else:
        response={}
        response["status"]="Failed"
        cluster.shutdown()
        return json.dumps(response)
    print "1 Executing query %s" % ("select * from %s where userid='%s'" % (table,id))
    rows=""    
    queried=True
    try:
        rows=session.execute("select * from %s where userid='%s'" % (table,id))
    except Exception as e:
        queried=False
    if not(queried):
        response={}
        response["status"]="Failed"
        response["reason"]="Failed to execute query"
        cluster.shutdown()
        return json.dumps(response)
    print "Executed query"
    matchingRow=""
    for row in rows:
        dbnid=""
        if channel=="inapp":
            dbnid=str(row.masternotificationid)
        else:
            dbnid=str(row.notificationid)
        print dbnid
        if dbnid==nid:
            matchingRow=row
            break
    print matchingRow
    if matchingRow=="":
        response={}
        response["status"]="Failed"
    else:
        response={}
        response["status"]="Success"
        response["data"]=getJsonFromRow(matchingRow,channel)
    cluster.shutdown()
    return json.dumps(response)

def getJsonFromRow(row,channel):
    jsondata={}
    if(channel=="inapp"):
        jsondata["userid"]=str(row.userid)
        jsondata["notificationid"]=str(row.masternotificationid)
        jsondata["actions"]=str(row.actions)
        jsondata["cdnurlforicon"]=str(row.cdnurlforicon)
        jsondata["cdnurlforimage"]=str(row.cdnurlforimage)
        jsondata["channels"]=str(row.channels)
        jsondata["notificationstatus"]=str(row.notificationstatus)
        jsondata["notificationsubtype"]=str(row.notificationsubtype)
        jsondata["notificationtext"]=str(row.notificationtext)
        jsondata["notificationtitle"]=str(row.notificationtitle)
        jsondata["notificationtype"]=str(row.notificationtype)
        jsondata["pageurl"]=str(row.urlforlandingpage)
        jsondata["starttime"]=str(row.starttime)
        jsondata["endtime"]=str(row.endtime)
        jsondata["heading"]=""
    else:
        jsondata["userid"]=str(row.userid)
        jsondata["notificationid"]=str(row.notificationid)
        jsondata["actions"]=""
        jsondata["cdnurlforicon"]=str(row.iconurl)
        jsondata["cdnurlforimage"]=str(row.imageurl)
        jsondata["channels"]=str(row.channels)
        jsondata["notificationstatus"]=""
        jsondata["notificationsubtype"]=str(row.subtype)
        jsondata["notificationtext"]=str(row.text)
        jsondata["notificationtitle"]=str(row.title)
        jsondata["notificationtype"]=str(row.type)
        jsondata["pageurl"]=str(row.pageurl)
        jsondata["starttime"]=""
        jsondata["endtime"]=str(row.endtime)
        jsondata["heading"]=str(row.heading)
    return jsondata

# Retrieve records from aerospike
# Parameter:
#     id: userid/deviceId, idType:user_id/device_id,namespace:namespace to refer,set: set to refer
@app.route('/notification/devicerecord/<host>/<port>/<namespace>/<set>/<id>/<idType>')
def get_deviceRecords(host,port,namespace,set,id,idType):
    namespace=namespace.lower()
    set=set.lower()
    idType=idType.lower()
    status={}
    config = {
        "hosts": [ (host, int(port)) ],
        "timeout": 1500
    }
    client=""
    records=""
    pp = pprint.PrettyPrinter(indent=2)
    p=aerospike.predicates
    try:
        client = aerospike.client(config).connect()
    except Exception as e:
        status["status"]="Failed"
        return json.dumps(status)
    print "connected"
    if idType.lower()=="user_id":
        print "createing Query for "+id
        try:
            status["status"]="Failed"
            status["data"]=[]
            for set in ["android","ios"]:
                query=client.query(str(namespace), str(set))
                print "Querying db"
                query.where(p.equals("user_id",str(id)))
                records=query.results( {'timeout':2000})
                pp.pprint(records)
                for record in records:
                    print record
                    _,_, rec=record
                    rec["platform"]=set
                    status["status"]="Success"
                    status["data"].append(rec)
        except Exception as e:
            status["status"]="Failed"
            print e
            client.close()
            return json.dumps(status)
    elif idType.lower()=="device_id":
        status["status"]="Failed"
        status["data"]=[]
        for set in ["android","ios"]:
            try:
                keys=(namespace,set,id)
                (key,meta,bins)=client.get((str(namespace), str(set),str(id)))
                print (key,meta,bins)
                rec=bins
                rec["platform"]=set
                print (rec)
                status["status"]="Success"
                status["data"].append(rec)
            except Exception as e:
                print "notfound"
    client.close()
    return json.dumps(status)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8082)

